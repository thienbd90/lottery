package com.vmg.services.delivery.math;

import java.sql.Time;

public class Utils {
	/**
	 * 
	 * @param time
	 * @param addSeconds
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Time addSeconds(Time time, int addSeconds) {
		int hours = time.getHours();
		int mins = time.getMinutes();
		int seconds = time.getSeconds();

		seconds += addSeconds;
		if (seconds >= 60) {
			mins += seconds / 60;
			seconds %= 60;

			if (mins >= 60) {
				hours += mins / 60;
				mins %= 60;
			}
		}
		return new Time(hours, mins, seconds);
	}
	/**
	 * 
	 * @param time1
	 * @param time2
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean compareTime(Time time1, Time time2) {
		long i1 = time1.getHours() * 3600 + time1.getMinutes() * 60 + time1.getSeconds();
		long i2 = time2.getHours() * 3600 + time2.getMinutes() * 60 + time2.getSeconds();
		return i1 - i2 <= 300L;
	}
	
	@SuppressWarnings("deprecation")
	public static int equal(Time time1, Time time2) {
		long i1 = time1.getHours() * 3600 + time1.getMinutes() * 60 + time1.getSeconds();
		long i2 = time2.getHours() * 3600 + time2.getMinutes() * 60 + time2.getSeconds();
		if(i1 > i2) return 1;
		if(i1 == i2) return 0;
		return -1;
	}
	
	public static Time subSeconds(Time time, int second) {
		int i1 = time.getHours() * 3600 + time.getMinutes() * 60 + time.getSeconds() - second;
		int hours = i1/3600;
		i1 %= 3600;
		
		int mins = i1/60;
		
		int seconds = i1%60;
		return new Time(hours, mins, seconds);
	}
}
