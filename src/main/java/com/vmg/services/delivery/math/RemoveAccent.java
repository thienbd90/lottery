/*     */ package com.vmg.services.delivery.math;
/*     */ 
/*     */ import java.io.FileInputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.Serializable;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.List;
/*     */ import java.util.Scanner;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class RemoveAccent
/*     */   implements Serializable
/*     */ {
/*     */   private static final long serialVersionUID = 1L;
/*  24 */   private static char[] SPECIAL_CHARACTERS = { 'À', 'À', 'Á', 'Â', 'Ã', 'È', 
/*  25 */     'É', 'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 
/*  26 */     'â', 'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 
/*  27 */     'ý', 'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 
/*  28 */     'Ạ', 'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 
/*  29 */     'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 
/*  30 */     'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 
/*  31 */     'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 
/*  32 */     'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 
/*  33 */     'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 
/*  34 */     'Ữ', 'ữ', 'Ự', 'ự' };
/*     */   
/*  36 */   static int[] map = new int['✐'];
/*  37 */   private static char[] REPLACEMENTS = { 'A', 'A', 'A', 'A', 'A', 'E', 'E', 
/*  38 */     'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a', 'a', 
/*  39 */     'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y', 
/*  40 */     'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u', 'A', 
/*  41 */     'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 
/*  42 */     'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e', 'E', 
/*  43 */     'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 
/*  44 */     'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 
/*  45 */     'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 
/*  46 */     'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 
/*  47 */     'u', 'U', 'u' };
/*     */   
/*     */   public RemoveAccent() {
/*  50 */     init();
/*     */   }
/*     */   
/*     */   private static void init() {
/*  54 */     for (int i = 0; i < SPECIAL_CHARACTERS.length; i++) {
/*  55 */       int x = SPECIAL_CHARACTERS[i];
/*  56 */       map[x] = i;
/*     */     }
/*     */   }
/*     */   
/*     */   public static void main(String[] args) {
/*  61 */     System.err.println(removeAccent("http://localhost:8080/delivery/delivery?data={%22start%22:{%22id%22:0,%22address%22:%2236%20Ho%C3%A0ng%20C%E1%BA%A7u,H%C3%A0%20N%E1%BB%99i,%C4%90%E1%BB%91ng%20%C4%90a%22,%22coordinate%22:null,%22time%22:%2211:10:02%22},%22trans%22:[{%22store%22:{%22id%22:0,%22address%22:%2236%20Ho%C3%A0ng%20C%E1%BA%A7u,H%C3%A0%20N%E1%BB%99i,%C4%90%E1%BB%91ng%20%C4%90a%22,%22coordinate%22:null,%22time%22:%2211:10:02%22},%22customers%22:[{%22id%22:1,%22address%22:%22620%20L%E1%BA%A1c%20Long%20Qu%C3%A2n,%20H%C3%A0%20N%E1%BB%99i%22,%22coordinate%22:null,%22time%22:%2212:10:00%22},{%22id%22:2,%22address%22:%22690%20L%E1%BA%A1c%20Long%20Qu%C3%A2n,%20H%C3%A0%20N%E1%BB%99i%22,%22coordinate%22:null,%22time%22:%2212:40:00%22}]},{%22store%22:{%22id%22:3,%22address%22:%22204A%20%C4%90%E1%BB%99i%20C%E1%BA%A5n%22,%22coordinate%22:null,%22time%22:null},%22customers%22:[{%22id%22:4,%22address%22:%2230%20%C4%90%E1%BB%99i%20C%E1%BA%A5n%22,%22coordinate%22:null,%22time%22:%2213:20:00%22},{%22id%22:5,%22address%22:%22191%20%C4%90%E1%BB%99i%20C%E1%BA%A5n%22,%22coordinate%22:null,%22time%22:%2213:50:00%22}]}],%22speed%22:80}"));
/*     */   }
/*     */   
/*     */ 
/*     */   public static String removeAccent(String s)
/*     */   {
/*  67 */     StringBuilder sb = new StringBuilder(s);
/*  68 */     for (int i = 0; i < sb.length(); i++) {
/*  69 */       sb.setCharAt(i, removeAccent(sb.charAt(i)));
/*     */     }
/*  71 */     return sb.toString();
/*     */   }
/*     */   
/*     */   public static char removeAccent(char ch) {
/*  75 */     int index = Arrays.binarySearch(SPECIAL_CHARACTERS, ch);
/*  76 */     if (index > 0) {
/*  77 */       ch = REPLACEMENTS[index];
/*     */     }
/*  79 */     return ch;
/*     */   }
/*     */   
/*     */   public static String trimString(String s) {
/*  83 */     return s.trim().replaceAll("\\s+", " ");
/*     */   }
/*     */   
/*     */   public static List<String> fileReader(String path) {
/*  87 */     List<String> list = new ArrayList<String>();
/*     */     try {
/*  89 */       FileInputStream is = new FileInputStream(path);
/*  90 */       Scanner input = new Scanner(is, "UTF-8");
/*  91 */       while (input.hasNextLine()) {
/*  92 */         String line = input.nextLine();
/*  93 */         if (line.equalsIgnoreCase("")) {}
/*     */         
/*  95 */         list.add(line.trim());
/*     */       }
/*  97 */       is.close();
/*  98 */       input.close();
/*     */     }
/*     */     catch (IOException e) {
/* 101 */       System.err.println(e.getMessage());
/*     */     }
/*     */     
/* 104 */     return list;
/*     */   }
/*     */ }