package com.vmg.services.delivery.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.util.*;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import vn.hus.nlp.lottery.entity.ILotery;
import vn.hus.nlp.lottery.entity.Type;
import vn.hus.nlp.lottery.parser.LotteryParser;


@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Value("${EMAIL_FILE}")
	String emailFile;

	@RequestMapping(value = { "/" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(1, 1, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "home";
	}

	LotteryParser parser = new LotteryParser();
	@RequestMapping(value = { "/lotterysms" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.GET }, produces = {
					"application/json; charset=UTF-8" })
	@ResponseBody
	public String generate(@RequestParam("data") String data) {
		parser = new LotteryParser(data);
		System.err.println(data);
		List<Type> rs = parser.parse();
		StringBuilder builder = new StringBuilder();
		for(Type t : rs) {
			builder.append(t.toString()).append("\n");
		}
		return builder.toString();
	}
	@RequestMapping(value = { "/generator" }, method = {
			org.springframework.web.bind.annotation.RequestMethod.POST }, produces = {
					"application/json; charset=UTF-8" })
	@ResponseBody
	public List<String> generate2(@RequestParam("keyword") String keyword) {
		try {
			keyword = URLDecoder.decode(keyword,"utf8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		parser = new LotteryParser(keyword);
		System.err.println(keyword);
		List<Type> rs = parser.parse();
		StringBuilder builder = new StringBuilder();
		for(Type t : rs) {
			builder.append(t.toString()).append("\n");
		}
		List<String> list = new ArrayList<>(Arrays.asList(builder.toString().split("\n")));
		return list;
	}
	
	String beautify(String json) {
	    ObjectMapper mapper = new ObjectMapper();
	    Object obj = null;
		try {
			obj = mapper.readValue(json, Object.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return "";
	}
}
