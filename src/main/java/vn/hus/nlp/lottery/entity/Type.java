package vn.hus.nlp.lottery.entity;

import java.util.ArrayList;
import java.util.List;

public class Type{

    private LType type;
    private String content;
    private List<String> tokens;
    private List<De> items;
    private List<Cang> cangs;
    private List<Lo> los;
    private List<Xien> xiens ;
    public Type() {
    }

    public Type(LType type, String content, List<String> tokens) {
        this.type = type;
        this.content = content.replaceAll(type.toString().toLowerCase(),"");
        this.tokens = tokens;
    }

    public List<De> getItems() {
        return items;
    }

    public void setItems(List<De> items) {
        this.items = items;
    }

    public Type(LType lType, String content) {
        this.type = lType;
        System.err.println("content = " + content);
        this.content = content.replaceAll(type.toString().toLowerCase(),"");
    }

    public List<Cang> getCangs() {
        return cangs;
    }

    public void setCangs(List<Cang> cangs) {
        this.cangs = cangs;
    }

    public List<Lo> getLos() {
        return los;
    }

    public void setLos(List<Lo> los) {
        this.los = los;
    }

    public List<Xien> getXiens() {
        return xiens;
    }

    public void setXiens(List<Xien> xiens) {
        this.xiens = xiens;
    }

    public LType getType() {
        return type;
    }

    public void setType(LType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }


    public String toString2() {
        return "Type{" +
                "type=" + type +
                ", content='" + content + '\'' +
                ", tokens=" + tokens +
                ", items=" + items +
                ", cangs=" + cangs +
                ", los=" + los +
                ", xiens=" + xiens +
                '}';
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(this.type.toString() + ":" + this.content + "\n");
        List<ILotery> objects;
        if(this.type.equals(LType.DE)) {
            builder.append(showList(items));
        } else if(this.type.equals(LType.CANG)) {
            builder.append(showList(cangs));
        } else if(this.type.equals(LType.LO)) {
            builder.append(showList(los));
        } else if(this.type.equals(LType.XIEN)) {
            builder.append(showList(xiens));
        }
        return builder.toString();
    }

    private String showList(List objs) {
        if(objs == null || objs.isEmpty()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for(Object o : objs) {
            builder.append(o.toString()).append(", ");
        }
        return builder.toString();
    }

    public double calculator(double deIn, double loIn, double xienIn) {
        double in = 0.0;
        if(this.type.equals(LType.DE)) {
            for(De d : items) {
                in+=Double.parseDouble(d.getMoney()) * deIn;
            }
        } else if(this.type.equals(LType.CANG)) {
            for(Cang c : cangs) {
                in=+Double.parseDouble(c.getMoney()) * deIn;
            }
        } else if(this.type.equals(LType.LO)) {
            for(Lo c : los) {
                in=+Double.parseDouble(c.getMoney()) * loIn;
            }
        } else if(this.type.equals(LType.XIEN)) {
            for(Xien c : xiens) {
                in=+Double.parseDouble(c.getMoney()) * xienIn;
            }
        }
        return in;
    }
}
