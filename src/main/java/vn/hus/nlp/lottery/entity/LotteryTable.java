package vn.hus.nlp.lottery.entity;

import java.util.List;

public class LotteryTable {
    private long date;
    private String spec;
    private List<String> others;

    public LotteryTable(long date, String spec, List<String> others) {
        this.date = date;
        this.spec = spec;
        this.others = others;
    }

    public LotteryTable() {
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public void setOthers(List<String> others) {
        this.others = others;
    }

    public List<String> getOthers() {
        return others;
    }

    public LotteryTable(String spec, List<String> others) {
        this.spec = spec;
        this.others = others;
    }

    @Override
    public String toString() {
        return "LotteryTable{" +
                "spec='" + spec + '\'' +
                ", others=" + others +
                '}';
    }
}
