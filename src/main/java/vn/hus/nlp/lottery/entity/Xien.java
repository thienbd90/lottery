package vn.hus.nlp.lottery.entity;

import java.util.List;

public class Xien extends ILotery {
    private List<String> choices;
    private String money;

    public Xien() {
    }

    public Xien( List<String> choices, String money) {
        this.choices = choices;
        this.money = money;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{choice: ").append(choices);
        sb.append(", money: ").append(money);
        sb.append('}');
        return sb.toString();
    }
}
