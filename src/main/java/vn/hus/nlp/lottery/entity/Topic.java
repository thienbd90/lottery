package vn.hus.nlp.lottery.entity;

import java.util.List;

public class Topic {
    private long date;
    private List<Agency> agencies;

    public Topic(long date, List<Agency> agencies) {
        this.date = date;
        this.agencies = agencies;
    }

    public Topic() {
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public List<Agency> getAgencies() {
        return agencies;
    }

    public void setAgencies(List<Agency> agencies) {
        this.agencies = agencies;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "date=" + date +
                ", agencies=" + agencies +
                '}';
    }
}
