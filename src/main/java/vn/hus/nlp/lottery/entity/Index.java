package vn.hus.nlp.lottery.entity;

public class Index implements  Comparable<Index> {
    private String label;
    private int index;

    public Index() {
    }

    public Index(String label, int index) {
        this.label = label;
        this.index = index;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Index{");
        sb.append("label='").append(label).append('\'');
        sb.append(", index=").append(index);
        sb.append('}');
        return sb.toString();
    }

    public int compareTo(Index o) {
        if(this.index > o.getIndex()) {
            return 1;
        }
        if(this.index == o.getIndex()) {
            return 0;
        }
        return -1;
    }
}
