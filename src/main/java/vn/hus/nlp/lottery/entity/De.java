package vn.hus.nlp.lottery.entity;

public class De extends ILotery{
    private String choice;
    private String money;

    public De() {
    }

    public De(String choice, String money) {
        this.choice = choice;
        this.money = money;
    }



    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{choice: ").append(choice);
        sb.append(", money: ").append(money);
        sb.append('}');
        return sb.toString();
    }
}
