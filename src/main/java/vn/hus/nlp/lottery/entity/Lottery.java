package vn.hus.nlp.lottery.entity;

import java.util.ArrayList;
import java.util.List;

public class Lottery {
    private String notif;
    private List<Type> types;

    public Lottery() {
        this.notif = "";
        this.types = new ArrayList<>();
    }

    public Lottery(String notif, List<Type> types) {
        this.notif = notif;
        this.types = types;
    }



    public String getNotif() {
        return notif;
    }

    public void setNotif(String notif) {
        this.notif = notif;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "Lottery{" +
                "notif='" + notif + '\'' +
                ", types=" + types + '}';
    }
}
