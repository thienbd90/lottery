package vn.hus.nlp.lottery.entity;

import java.util.List;

public class Person {
    private String mobile;
    private String name;
    private List<Type> types;

    public Person() {
    }

    public Person(String mobile, String name, List<Type> types) {
        this.mobile = mobile;
        this.name = name;
        this.types = types;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "Person{" +
                "mobile='" + mobile + '\'' +
                ", name='" + name + '\'' +
                ", types=" + types +
                '}';
    }
}
