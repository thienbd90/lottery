package vn.hus.nlp.lottery.entity;

import java.util.List;

public class Agency {
    private double deIn;
    private double loIn;
    private double xienIn;

    private List<Person> persons;

    public Agency() {
    }

    public Agency(double deIn, double loIn, double xienIn, List<Person> persons) {
        this.deIn = deIn;
        this.loIn = loIn;
        this.xienIn = xienIn;
        this.persons = persons;
    }

    public double getDeIn() {
        return deIn;
    }

    public void setDeIn(double deIn) {
        this.deIn = deIn;
    }

    public double getLoIn() {
        return loIn;
    }

    public void setLoIn(double loIn) {
        this.loIn = loIn;
    }

    public double getXienIn() {
        return xienIn;
    }

    public void setXienIn(double xienIn) {
        this.xienIn = xienIn;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public String toString() {
        return "Agency{" +
                "deIn=" + deIn +
                ", loIn=" + loIn +
                ", xienIn=" + xienIn +
                ", persons=" + persons +
                '}';
    }
}
