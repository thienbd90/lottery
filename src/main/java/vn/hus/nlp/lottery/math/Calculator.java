package vn.hus.nlp.lottery.math;

import vn.hus.nlp.lottery.entity.*;
import vn.hus.nlp.lottery.parser.KQCrawler;

import java.util.List;

public class Calculator {

    private static LotteryTable table = null;
    private static long date;

    private static double getIncome(Topic topic) {
        if(table == null) {
            table = KQCrawler.crawlDay(null);
        }
        double income = 0.0;

        for(Agency agency : topic.getAgencies()) {
            double deIn = agency.getDeIn();
            double loIn = agency.getLoIn();
            double xienIn = agency.getXienIn();
            for(Person person : agency.getPersons()) {
                List<Type> types = person.getTypes();
                for(Type type : types) {
                    income+=type.calculator(deIn,loIn,xienIn);
                }
            }
        }
        return income;
    }

    
}
