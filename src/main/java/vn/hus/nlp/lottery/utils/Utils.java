package vn.hus.nlp.lottery.utils;
import com.sun.org.apache.xpath.internal.operations.Lt;
import sun.security.krb5.internal.crypto.EType;
import vn.hus.nlp.lottery.entity.Index;
import vn.hus.nlp.lottery.entity.LType;
import vn.hus.nlp.lottery.entity.Type;

import java.lang.reflect.Array;
import java.util.*;

public class Utils {
     private static Map<String, List<String>> lmap = new HashMap<>();


     static {
         loadMap();
     }

    private static void loadMap() {
         lmap.put("cang",Arrays.asList("càng","3cang","3 cang","bacang","ba cang"));
         lmap.put("de", Arrays.asList("đề","de","danh de","ghi đe"));
         lmap.put("lo",Arrays.asList("lô","ghi lo","lo","loto"));
         lmap.put("xien",Arrays.asList("xien","lo xien","lô xiên"));
         lmap.put("xien quay",Arrays.asList("xiên quay","xien quay","xienquay"));
    }

    public static String normalize(String sms) {

        List<String> set = new ArrayList<>(lmap.keySet());
        System.err.println(set);
        Collections.sort(set, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.length() > o2.length() )return -1;
                if(o1.length()==o2.length()) return 0;
                return 1;
            }
        });

        System.err.println(set);

        System.err.println(sms);
                sms = sms.trim().replaceAll("O", "0");
        sms = RemoveAccent.removeAccent(sms).toLowerCase();
        sms = sms.trim().replaceAll("\\s+"," ");
        sms = sms.trim().replaceAll("x"," x");
        sms = sms.trim().replaceAll("x\\s+","x");

        sms = sms.trim().replaceAll("\\strieu","trieu");
        sms = sms.trim().replaceAll("\\sdiem","diem");
        sms = sms.trim().replaceAll("\\snghin","nghin");
        sms = sms.trim().replaceAll("[0-9]+\\s*(c|con|so)", "");
        return sms.trim();
    }
    public static List<Integer> sort(int ...arr) {
        List<Integer> list = new ArrayList<Integer>();
        for(int e : arr) {
            list.add(e);
        }
        Collections.sort(list);
        return list;
    }



    public static List<Index> indexOf(List<String> labels, String sms) {
        List<Index> list = new ArrayList<Index>();
        for(String label : labels) {
            int index = sms.indexOf(label);
            if(index > -1) {
                list.add(new Index(label,index));
                while (index >= 0) {
                    index = sms.indexOf(label, index + label.length());
                    if (index > -1) {
                        list.add(new Index(label, index));
                    }
                }
            }
        }
        Collections.sort(list);
        return list;
    }

    public static LType enumOf(String label) {
        switch (label) {
            case "lo" :
                return LType.LO;
            case "de" :
                return LType.DE;
            case "cang" :
                return LType.CANG;
            case "xien" :
                return LType.XIEN;
            case "xienquay" :
                return LType.XIENQUAY;
                default:
                    return LType.KHAC;
        }
    }

    public static boolean isContainDigit(String token) {
        for(Character c : token.toCharArray()) {
            if(Character.isDigit(c)) {
                return true;
            }
        }

        if(token.contains("dd") || token.contains("đđ") || token.contains("dau") || token.contains("duoi") || token.contains("dit")) {
            return true;
        }
        return false;
    }

    /*public static boolean isCUnit(String token) {
        if(token.matches("(trieu|nghin|tram|diem|d)"))
    }*/

    public static List<String> generation(String token) {
        token = token.trim();
        List<String> result = new ArrayList<>();
        if(token.trim().length() == 1) {
            result.add("0"+token);
        } else if(token.length() >2) {
            for(int i = 0 ; i < token.length() - 1; ++i) {
                result.add(token.charAt(i) + "" + token.charAt(i+1));
            }
        } else {
            result.add(token);
        }
        return result;
    }

    public static String removeUnit(String money) {
        money = money.replaceAll("(nghin|ngin|canh|n|k|ngh|ng)","000");
        money = money.replaceAll("(triệu|trieu)","000000");
        money = money.replaceAll("(diem|dm|d|₫)","");
        money = money.trim().replaceAll("x","");
        money = money.trim().replaceAll("\\*","");

        if(Integer.parseInt(money) < 20000) money = money +"000";
        return money;
    }

    public static String count(List<String> tmp) {
        int i = 0 ;
        for ( ;i < tmp.size() ; ++i) {}
        return i -1 +"";
    }

    public static List<String> subList(List<String> tmp, int s, int end) {
        List<String> list = new ArrayList<>();
        for (int i = s ; i < end ; ++i) {
            list.add(tmp.get(i));
        }
        return list;
    }

    public static boolean listIsContain(String[] strings, String s) {
        for(String token : strings) {
            if(token.equalsIgnoreCase(s)){
                return true;
            }
        }
        return false;
    }

    public static boolean isDigit(String choice) {
        for(Character c : choice.toCharArray()) {
            if(!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }
}
