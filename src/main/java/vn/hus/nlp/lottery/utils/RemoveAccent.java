package vn.hus.nlp.lottery.utils;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;


/**
 * @author Duc-Thien , BUIthienbd90@gmail.com
 *         <p>
 *         Jan 15, 2015, 11:50:28 PM
 *         <p>
 *         This is the general remove  for texts. accent. All the texts are supposed
 *         to be encoded in UTF-8 encoding.
 */
public class RemoveAccent implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * All unicode characters of Vietnamese
	 */
	private static char[] SPECIAL_CHARACTERS = { 'À', 'À', 'Á', 'Â', 'Ã', 'È',
			'É', 'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á',
			'â', 'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú',
			'ý', 'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư',
			'Ạ', 'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ',
			'ậ', 'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ',
			'Ẻ', 'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ',
			'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ',
			'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ',
			'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử',
			'Ữ', 'ữ', 'Ự', 'ự' };

	private static int[] map = new int[10000];

	/**
	 * All characters of ANSI
	 */
	private static char[] REPLACEMENTS = { 'A', 'A', 'A', 'A', 'A', 'E', 'E',
			'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a', 'a',
			'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y',
			'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u', 'A',
			'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a',
			'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e', 'E',
			'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e',
			'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
			'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
			'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U',
			'u', 'U', 'u' };



	/**
	 * This function to remove accent of text
	 * @param s
	 * @return
	 */
	public static String removeAccent(String s) {
		// s = trimString(s);

		StringBuilder sb = new StringBuilder(s);
		for (int i = 0; i < sb.length(); i++) {
			sb.setCharAt(i, removeAccent(sb.charAt(i)));
		}
		return sb.toString();
	}

	/**
	 * This function to remove accent of character
	 * @param ch
	 * @return
	 */
	public static char removeAccent(char ch) {
		int index = Arrays.binarySearch(SPECIAL_CHARACTERS, ch);
		if (index > 0) {
			ch = REPLACEMENTS[index];
		}
		return ch;
	}

	/**
	 * The normalize text input. Remove white spaces at begin and end of text
	 * @param s
	 * @return
	 */
	public static String trimString(String s) {
		return s.trim().replaceAll("\\s+", " ");
	}

	/**
	 * Remove accent all texts of file
	 * @param inputFile
	 * @param outputFile
	 * @return
	 * @throws IOException
	 */
	public static void removeAccent(String inputFile, String outputFile) throws IOException {
		/*SentenceDetector sentenceDetector = new SentenceDetector("models/sentDetection//VietnameseSD.bin.gz");
		String[]  sources = sentenceDetector.detectSentences(inputFile);
		UTF8FileUtility.createWriter(outputFile);
		for(String sent : sources) {
			UTF8FileUtility.writeln(removeAccent(sent)+"\n");
		}
		UTF8FileUtility.closeWriter();*/
	}

}
