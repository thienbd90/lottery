package vn.hus.nlp.lottery.parser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import vn.hus.nlp.lottery.entity.LotteryTable;

public class KQCrawler {
    private static ArrayList<String> listKeys = new ArrayList<>();;
    private static String dateID = "result_date";
    private static final String BASE_URL = "http://ketqua.net/xo-so-truyen-thong.php";

    private static long getResultTime(String text) throws ParseException {
        String dateRegex = "([0-9]{2}-[0-9]{2}-[0-9]{4})";

        Pattern p = Pattern.compile(dateRegex);

        Matcher m = p.matcher(text);
        if (m.find()) {
            System.out.println(m.group());
            Date resultDay = new SimpleDateFormat("dd-MM-yyyy").parse(m.group());
            return resultDay.getTime();
        }
        return -1;
    }

    public static LotteryTable crawlDay(String date) {
        if (listKeys.size() == 0) {
            listKeys.add("rs_0_0");
            listKeys.add("rs_1_0");
            listKeys.add("rs_2_0");
            listKeys.add("rs_2_1");
            listKeys.add("rs_3_0");
            listKeys.add("rs_3_1");
            listKeys.add("rs_3_2");
            listKeys.add("rs_3_3");
            listKeys.add("rs_3_4");
            listKeys.add("rs_3_5");
            listKeys.add("rs_4_0");
            listKeys.add("rs_4_1");
            listKeys.add("rs_4_2");
            listKeys.add("rs_4_3");
            listKeys.add("rs_5_0");
            listKeys.add("rs_5_1");
            listKeys.add("rs_5_2");
            listKeys.add("rs_5_3");
            listKeys.add("rs_5_4");
            listKeys.add("rs_5_5");
            listKeys.add("rs_6_0");
            listKeys.add("rs_6_1");
            listKeys.add("rs_6_2");
            listKeys.add("rs_7_0");
            listKeys.add("rs_7_1");
            listKeys.add("rs_7_2");
            listKeys.add("rs_7_3");
        }

        LotteryTable lottery = null;
        try {
            String url = date == null ? BASE_URL : BASE_URL + "?ngay=" + date;
            Document doc = Jsoup.connect(url).data("query", "Java").userAgent("Mozilla").cookie("auth", "token")
                    .timeout(3000).get();

            String dateString = doc.getElementById(dateID).text();
            long resultDateLong = getResultTime(dateString);

            Element dacBiet = doc.getElementById(listKeys.get(0));
            String spec = dacBiet.text().replace(" ", "");
            if (!(spec.trim().length() > 0)) {
                spec = "NaN";
            }

            List<String> others = new ArrayList<>();
            for (int j = 1; j < listKeys.size(); j++) {
                try {
                    Element numberElement = doc.getElementById(listKeys.get(j));
                    String number = numberElement.text().replace(" ", "").trim();
                    if (number.length() > 0) {
                        others.add(number);
                    } else {
                        others.add("NaN");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }

            if (resultDateLong != -1) {
                lottery = new LotteryTable(resultDateLong, spec, others);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lottery;
    }

    public static void main(String[] args) {
        long s = System.currentTimeMillis();
        System.out.println(crawlDay(null));
        System.out.println(System.currentTimeMillis() - s);
    }
}
