package vn.hus.nlp.lottery.parser;

import sun.security.krb5.internal.crypto.EType;
import vn.hus.nlp.lottery.entity.*;
import vn.hus.nlp.lottery.utils.StringRules;
import vn.hus.nlp.lottery.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LotteryParser {

    private static List<String> labels = Arrays.asList(new String[]{"de", "lo", "xien", "cang"});
    private static String TOKENS_SPARATOR = "[\\s-_|,|.|\\*/]+";

    private String content;
    private List<Type> lotteries;


    public LotteryParser() {
        this.content = "";
        this.lotteries = new ArrayList<>();
    }

    public LotteryParser(String content) {
        this.content = Utils.normalize(content).toLowerCase();
        this.lotteries = new ArrayList<>();
    }

    public List<Type> parse() {
        List<String> labels = Arrays.asList(new String[]{"de", "lo", "xien", "cang", "xienquay"});
        List<Type> types = typesOf(this.content, labels);
        System.err.println(types);
        types.forEach(item -> {
            if(item.getType().equals(LType.DE)) {
                makeDe(item);
            } else if (item.getType().equals(LType.CANG)) {
                makeCang(item);
            } else if(item.getType().equals(LType.LO)){
                makeLo(item);
            } else if(item.getType().equals(LType.XIEN)){
                makeXien(item);
            }
        } );
        return this.lotteries;
    }




    public List<Type> typesOf(String sms, List<String> lables) {
        List<Type> types = new ArrayList<Type>();
        List<Index> indexes = Utils.indexOf(lables, sms);
        if(indexes.size() == 1) {
            Type type = new Type(Utils.enumOf(indexes.get(0).getLabel()), sms.substring(indexes.get(0).getIndex()));
            type.setTokens(split(type.getContent()));
            types.add(type);
        } else {
            for (int i = 0 ; i < indexes.size() - 1 ; ++i) {
                Type type = new Type(Utils.enumOf(indexes.get(i).getLabel()), sms.substring(indexes.get(i).getIndex(),indexes.get(i+1).getIndex()));
                type.setTokens(split(type.getContent()));
                types.add(type);
            }
            Type type = new Type(Utils.enumOf(indexes.get(indexes.size()-1).getLabel()), sms.substring(indexes.get(indexes.size()-1).getIndex()));
            type.setTokens(split(type.getContent()));
            types.add(type);
        }
        System.err.println("===========sadfas==== " + types.get(0).toString2());
        return types;
    }


    // 2345 : 23 3445

    private void makeDe(Type type) {

        System.err.println("ddddddddddđ = " + type.toString2() + " 0000 ");
        List<De> des = new ArrayList<>();
        List<String> tmp = new ArrayList<>();
        for(int i = 0; i < type.getTokens().size(); ++i) {
            String token = type.getTokens().get(i);
            if(isBet(token)) {
                for(String t : tmp) {
                    des.addAll(processFandE(new De(t,token)));
                }
                tmp.clear();
            } else {
                tmp.add(token);
            }
        }
        type.setItems(des);
        this.lotteries.add(type);
    }

    private void makeLo(Type type) {
        List<Lo> los = new ArrayList<>();
        List<String> tmp = new ArrayList<>();
        for(int i = 0; i < type.getTokens().size(); ++i) {
            String token = type.getTokens().get(i);
            if(isBet(token)) {
                for(String t : tmp) {
                    los.addAll(processLos(new Lo(t,token)));
                }
                tmp.clear();
            } else {
                tmp.add(token);
            }
        }
        type.setLos(los);
        this.lotteries.add(type);
    }

    private void makeCang(Type type) {
        List<Cang> cangs = new ArrayList<>();
        List<String> tmp = new ArrayList<>();
        for(int i = 0; i < type.getTokens().size(); ++i) {
            String token = type.getTokens().get(i);
            if(isBet(token)) {
                for(String t : tmp) {
                    cangs.add((new Cang(t,Utils.removeUnit(token))));
                }
                tmp.clear();
            } else {
                tmp.add(token);
            }
        }
        type.setCangs(cangs);
        this.lotteries.add(type);
    }

    private void makeXien(Type type) {
        List<Xien> xiens = new ArrayList<>();
        List<String> tmp = new ArrayList<>();
        int count = 0;
        for(int i = 0; i < type.getTokens().size(); ++i) {
            String token = type.getTokens().get(i);
            if(isBet(token)) {
                boolean flag = false;
                if(tmp.get(0).length() < 2 ) {
                    count = Integer.parseInt(tmp.get(0));
                    flag = true;
                }
                if(count != 0) {
                    List<String> temp = new ArrayList<String>(tmp);
                    for(int j = flag ? 1 : 0  ; j < tmp.size();) {
                        List<String> choices = temp.subList(j, j+count);
                        xiens.add(new Xien(choices, Utils.removeUnit(token)));
                        j+=count;
                    }
                }
                tmp.clear();
            } else {
                tmp.add(token);
            }
        }
        type.setXiens(xiens);
        this.lotteries.add(type);
    }




    private List<String> split(String str) {
        String[] tokens = str.split(TOKENS_SPARATOR);
        List<String> results = new ArrayList<String>();
        String tag = "";

        for (int i = 0; i < tokens.length; ++i) {
            if(tokens[i].isEmpty() || !Utils.isContainDigit(tokens[i])) continue;
            if (tokens[i].matches("(dd|đđ|dau|dit|duoi)")) {
                tag = tokens[i];
            } else {
                if(!isBet(tokens[i])) {
                    results.add(tag + tokens[i]);
                } else {
                    results.add(tokens[i]);
                }
            }
        }
        System.err.println("tokens====== " + results);
        return results;
    }


    private static boolean isBet(String token) {
        if (token.matches("[0-9]+(\\s)*(trieu|tr|₫|d|diem|d|nghin|ngin|k|n)") && !token.startsWith("0")) {
            return true;
        }
        if (token.matches("(x|\\*)[0-9]+")) {
            return true;
        }
        if (token.matches("(x|\\*)[0-9]+(\\s)*(trieu|tr|₫|d|diem|d|nghin|ngin|k|n)")) {
            return true;
        }
        return false;
    }


    public List<De> processFandE(De item) {
        List<De> des = new ArrayList<>();
        //System.err.println("item.choice = " + item.getChoice());
        String unit = "";
        if(item.getChoice().length() > 2 && Utils.isDigit(item.getChoice()) ) {
            //if(item.getChoice())
            List<String> choices = Utils.generation(item.getChoice());
            for(String c : choices) {
                des.add(new De(c, Utils.removeUnit(item.getMoney())));
            }
        } else if(item.getChoice().matches("(dau|đầu|₫ầu)\\d")) {
            for(int i = 0 ; i < 10; ++i) {
                des.add(new De(item.getChoice()+i, Utils.removeUnit(item.getMoney())));
            }
        } else if(item.getChoice().matches("(dit|đít|₫ít|duoi|đuôi)\\d")) {
            for(int i = 0 ; i < 10; ++i) {
                des.add(new De(i+item.getChoice(), Utils.removeUnit(item.getMoney())));
            }
        } else if(item.getChoice().matches("(dd|đđ)\\d")) {
            //System.err.println("đầu - đít");
            unit = item.getChoice().replaceAll("(đđ|dd)","");

            for(int i = 0 ; i < 10; ++i) {
                des.add(new De(i+unit,Utils.removeUnit(item.getMoney())));
                if(!item.getChoice().equalsIgnoreCase(i+"")) des.add(new De(unit+i,Utils.removeUnit(item.getMoney())));
            }
        }else {
            item.setMoney(Utils.removeUnit(item.getMoney()));
            des.add(item);
        }
        return des;
    }

    public List<Lo> processLos(Lo item) {

        List<Lo> los = new ArrayList<>();
        //System.err.println("item.choice = " + item.getChoice());
        String unit = "";
        if(item.getChoice().length() > 2 && Utils.isDigit(item.getChoice()) ) {
            List<String> choices = Utils.generation(item.getChoice());
            for(String c : choices) {
                los.add(new Lo(c, Utils.removeUnit(item.getMoney())));
            }
        } else if(item.getChoice().matches("(dau|đầu|₫ầu)\\d")) {
            for(int i = 0 ; i < 10; ++i) {
                los.add(new Lo(item.getChoice()+i, Utils.removeUnit(item.getMoney())));
            }
        } else if(item.getChoice().matches("(dit|đít|₫ít|duoi|đuôi)\\d")) {
            for(int i = 0 ; i < 10; ++i) {
                los.add(new Lo(i+item.getChoice(), Utils.removeUnit(item.getMoney())));
            }
        } else if(item.getChoice().matches("(dd|đđ)\\d")) {
            //System.err.println("đầu - đít");
            unit = item.getChoice().replaceAll("(đđ|dd)","");

            for(int i = 0 ; i < 10; ++i) {
                los.add(new Lo(i+unit,Utils.removeUnit(item.getMoney())));
                if(!item.getChoice().equalsIgnoreCase(i+"")) los.add(new Lo(unit+i,Utils.removeUnit(item.getMoney())));
            }
        }else {
            item.setMoney(Utils.removeUnit(item.getMoney()));
            los.add(item);
        }
        return los;
    }


    public static void main(String[] args) {
        String sms = "Để 454 x 500 lo 34,34,44 x300";
        LotteryParser parser = new LotteryParser(sms);
        List<Type> types = parser.parse();
        double income = 0.0;
        for(Type type : types) {
            System.err.println(type.toString() + ", Income = " + type.calculator(1.0,24.0,24.0));
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Type> getLotteries() {
        return lotteries;
    }

    public void setLotteries(List<Type> lotteries) {
        this.lotteries = lotteries;
    }

    @Override
    public String toString() {
        System.err.println("Start...");
        for(Type t : lotteries) {
            System.err.println(t.getType());
            System.err.println(t.getContent());
            System.err.println(t.getTokens());

            if(t.getItems()!=null) System.err.println(t.getItems());
            if(t.getCangs()!=null) System.err.println(t.getCangs());
            if(t.getLos()!=null) System.err.println(t.getLos());
            if(t.getXiens()!=null) System.err.println(t.getXiens());
            System.err.println("================================");

        }
        return "";
    }
}
