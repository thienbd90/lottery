<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html ng-app="app">
<head>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="resources/favicon.ico">
<title>Lottery</title>
<meta name="description"
	content="A free responsive HTML 5 template with a clean style.">
<meta name="keywords"
	content="free template, html 5, responsive, clean, scss">
<link rel="apple-touch-icon"
	href="resources/images/touch/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="resources/images/touch/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="resources/images/touch/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="resources/images/touch/apple-touch-icon-144x144.png">
<meta property="og:title" content="">
<meta property="og:description" content="">
<meta property="og:url" content="">
<meta property="og:image" content="">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="address=no">
<link rel="stylesheet"
	href="resources/bower_components/bootstrap/dist/css/bootstrap.min.css">

<link rel="stylesheet" href="resources/css/form.css">
<link rel="stylesheet" href="resources/css/scroller.css">
<link rel="stylesheet" href="resources/css/style.css">
<link rel="stylesheet" href="resources/css/spinner.css">
<link rel="stylesheet" href="resources/css/table.css">

<link rel="stylesheet" type="text/css"
	href="resources/css/normalize.css" />
<!--  <link rel="stylesheet" type="text/css" href="resources/css/demo.css" /> -->
<link rel="stylesheet" type="text/css"
	href="resources/css/ns-default.css" />
<link rel="stylesheet" type="text/css"
	href="resources/css/ns-style-other.css" />
<script src="resources/js/modernizr.custom.js"></script>

</head>
<body>
	<%--ng-controller="MainController"--%>
	<div class="main" ng-controller="MainController">
		<header>
			<section class="hero">
				<!--<div class="texture-overlay"></div>-->
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h1 class="animated fadeInDown">Let's change</h1>
						</div>
					</div>
				</div>
			</section>
		</header>
		<section class="" id="about" style="margin-top: 100px;">
			<div class="">
				<div class="cd-form floating-labels">
					<div class="info-message">
						<p style="font-size: 120%;">Trải nghiệm</p>
					</div>
					<div id="recommend">
						<div class="wrapper">
							<div class="scene">
								<div>
									<span id="vader"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="icon">
						<!--<label class="cd-label" for="cd-name"></label>  -->
						<input class="user" type="text" name="keyword" id="keyword"
							placeholder="data..." required
							ng-keypress="inputKeyPress($event)">
					</div>
					<div style="text-align: center">
						<input class=" text-center" type="button" value="OK"
							ng-click="getSentences()" />
					</div>
				</div>
				<div id="demo" class="hidden"
					style="text-align: center; max-width: 600px;">
					<div class="mCustomScrollbar content fluid light"
						data-mcs-theme="inset-2-dark">
						<div id="result">
							<table>
								<tbody>
									<tr ng-repeat="item in data.items">
										<td>{{item}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="loading" class="spinner hidden">
					<div class="dot1"></div>
					<div class="dot2"></div>
				</div>
			</div>
		</section>
	</div>
	<!-- /.main -->
	<footer>
		<div class="wrap">
			<p>
				&copy; 2016 <strong>VMG</strong>, All Rights Reserved
			</p>
		</div>
		<!-- /.wrap -->
	</footer>

	<script src="resources/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="resources/bower_components/angular/angular.min.js"></script>
	<script src="resources/js/app/services.js"></script>
	<script src="resources/js/app/controllers.js"></script>
</body>
</html>