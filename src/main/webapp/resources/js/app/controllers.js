/**
 * Created by thienbd on 8/25/15.
 */
app.controller('MainController',function($scope,$sce, dataService) {
    $scope.data = {};
    $scope.haveResult = false;


    $scope.getSentences = function () {
        var keyword = $("#keyword").val();
        /*alert(keyword);*/
        console.log(keyword);
        $('#demo').addClass('hidden');
        $('#loading').removeClass('hidden');
        dataService.getSentences(keyword)
            .success(function (result) {
                $scope.data.items = result;
                $("#loading").addClass('hidden');
                $('#demo').removeClass('hidden');

            })
            .error(function (result) {
            	alert("OK");
                alert("Có lỗi xảy ra. Bạn hãy thử lại nhé!");
            })

    }

    $scope.inputKeyPress = function (e){
        if (e.keyCode == 13) {
            $scope.getSentences();
        }
    }
})