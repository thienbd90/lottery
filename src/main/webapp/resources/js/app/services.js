/**
 * Created by thienbd on 8/25/15.
 */
var app = angular.module("app",[]);

app.service('dataService', function ($http) {

    this.getSentences = function(keyword){
        return $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            data: 'keyword=' + encodeURIComponent(keyword),
            url: "generator",
            timeout: 10000
        });
    }

})
